<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hQQo/W3pGuA5/5WhtkRv8d+SllIZdpaS3avh/5tir9LVCzk6PTHEh0GIMbgaPGfWYY2xquQ+0Hja+y502z2ucQ==');
define('SECURE_AUTH_KEY',  'KcfSpGI0BhzST5XhM0hQLlo0moYchCcD08HnpPiycbydTmxdSd4Jx34/gSvpIbN0cQrAsqVXtG3XoglDMFXc6w==');
define('LOGGED_IN_KEY',    'RnNkr4EKnqHnpakbtS3GHHCXhocNT1i8f0m1VD+5XKHswouoh15dRjlQ6qVnC7qSQsSArcxJzvJdOH5dsjaPBw==');
define('NONCE_KEY',        '8MnhfkfnQHqiLAt7mq1uKq9RHehDI6lca+DFUKqNthJCaEQP7m7ICBQiv8Oys1rfRxDqIA156A8F38D23j2lsg==');
define('AUTH_SALT',        'QqPriIMD6UtxM84pvroe3EeE4QaV9Dgkw18r7eDnikfcRHZYbSLTTqear/t/wFOaan6gigu/giPiHNhbK7Jc5g==');
define('SECURE_AUTH_SALT', 'X6FTjiovWIiVli2LTwcNa0VuotkrxWdFX16/gmeAA1iYI7FhL323+6YcnTIpwMyw9mmP8QUyEPw2hwveqLrlmA==');
define('LOGGED_IN_SALT',   'LXc1VjdgkMuqvjFgHOFsg/K6XOLt5Z57hfuI2SbVYAtylflByWAYMvoxWsgFFrlhIQh1qlaBmR9uSXHGei0S8Q==');
define('NONCE_SALT',       'uz6vaSKrtNoqtlKAEXazYewgeGq3c6VCLiPIDI1lqG0qt6LdBBA50caQrAdhwsTff5xD3YUVMqf4o0a/MtCQbw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
